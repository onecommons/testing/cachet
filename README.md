# Application Blueprint for Cachet

## What is Cachet?
Cachet is an open source status page system for everyone.

**Documentation at:** [https://docs.cachethq.io/docs](https://docs.cachethq.io/docs)

📛 An open source status page system for everyone.

**GitHub:** <https://github.com/CachetHQ/Cachet>

This Application Blueprint is made possible by the [Community Maintained One Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from [https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/cachet.yml](https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/cachet.yml).  Changes can instead be applied to `ensemble-template.yaml` to overwrite the generated values.
